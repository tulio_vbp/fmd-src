function [x_pitch, x_cam, theta,rho] = cam_DesignTrans(motionLaw,Rb,Rr)
% INPUT
% - y: motionLaw
% - baseCircle: struct with center .c and radius .r
% - Rr: Roller radius
% - b: follower length
% - d: frame length
% ---
% OUTPUT
% - x_pitch: pitch curve in cartesian coordinates
% - x_cam: cam profile in cartesian coordinates
% - theta: pressure angle as function of alfa
% - rho: curvature radius as function of alfa

%beta0 = acos((b^2+d^2-(Rb+Rr)^2)/(2*b*d)); % rad
% Motion law data - Assuming abscissa to be in degrees
y = motionLaw.pos;
yp = motionLaw.vel;%*180/pi; % dy/dalfa' = dy/dalfa*dalfa/dalfa';
ypp = motionLaw.acc;%*(180/pi)^2; % d2y/dalfa'2 = dy/dalfa*(dalfa/dalfa')^2;

da = 360/(length(y)-1);
alfa = (0:da:360)/180*pi; % Angular displacement of the cam

% Polar coordinates of the pitch curve 
rc = Rb + y;
phi_c = alfa;
% Cartesian coordinates of the pitch curve
x_pitch = [(rc.*cos(phi_c))' (rc.*sin(phi_c))'];

% Pressure angle
theta = atan(yp./(Rb + y));

% Polar coordinates of the cam profile
r = sqrt(Rr^2 + (Rb+y).^2 - 2*Rr*(Rb+y).*cos(theta));
phi = alfa + asin(Rr*sin(theta)./r);
% Cartesian coordinates of the cam curve
x_cam = [(r.*cos(phi))' (r.*sin(phi))'];


% Curvature radius of the cam profile

rho0 = (yp.^2 + (Rb+y).^2).^(3/2)./((Rb+y).^2 - (Rb+y).*ypp + 2*yp.^2);
rho = rho0 - Rr;


end