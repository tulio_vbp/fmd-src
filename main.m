%% Main
clear all
clc
close all

%% Data
% Cam 1
%d = 30;     % Frame length [mm]
%b = 30;     % Follower length [mm]
Rb = 300;   % Base radius [mm]
Rr = 10;     % Roller radius [mm]

% Cam 2
d = 30;     % Frame length [mm]
b = 30;     % Follower length [mm]
Rb_q = 20;   % Base radius [mm]
Rr_q = 10;     % Roller radius [mm]


alfa = 0:1:360; %[deg]

%% Motion law
MotionLaw_x = {'CIC' 'CIC' 'Dwell' 'CIC' 'CIC'}; %A cell array is built
MotionLaw_theta = {'Dwell' 'CIC' 'Dwell' 'CIC' 'Dwell'}; 
LiftTheta_deg = [0 -90 0 90 0];
LiftTheta = deg2rad(LiftTheta_deg);
LiftX = [-160 -20 0 20 160];
Abscissa = [100 30 40 30 160]; %duration of each phase
x = LeggeDiMotoV2(MotionLaw_x,LiftX,Abscissa,length(alfa),[]);
q = LeggeDiMotoV2(MotionLaw_theta,LiftTheta,Abscissa,length(alfa),[]);

figure
subplot(3,1,1)
plot(alfa,x.pos,'LineWidth',1.5);
hold on
plot(alfa,180/pi*q.pos,'LineWidth',1.5);
xlim([0 max(alfa)])
ylabel('Disp. (-)');
title('Motion Law');
grid;
set(gca,'FontSize',12)
legend('x (mm)','\theta (deg)')

subplot(3,1,2)
plot(alfa,x.vel,'LineWidth',1.5);
hold on
plot(alfa,180/pi*q.vel,'LineWidth',1.5);
xlim([0 max(alfa)])
ylabel('Vel.(-/deg)');
grid;
set(gca,'FontSize',12)

subplot(3,1,3)
plot(alfa,x.acc,'LineWidth',1.5);
hold on
plot(alfa,180/pi*q.acc,'LineWidth',1.5);
xlim([0 max(alfa)])
ylabel('Acc. (-/deg^2)');
xlabel('\alpha (degrees)');
grid;
set(gca,'FontSize',12)

%% Cam design
% Fixing the derivatives
x.vel = x.vel*(180/pi);
x.acc = x.acc*(180/pi)^2;
q.vel = q.vel*(180/pi);
q.acc = q.acc*(180/pi)^2;

[x_pitch, x_cam, x_theta,x_rho] = cam_DesignTrans(x,Rb,Rr);
%[q_pitch,q_cam,q_theta,q_rho] = cam_DesignRot(motionLaw,Rb_q,Rr_q,b,d);
x_rho0 = x_rho + Rr;
x_undercut = x_rho0.*x_rho;

% Plot theta
figure
plot(alfa,x_theta*180/pi,'LineWidth',1.5)
ylabel('Pressure angle \theta (deg)')
xlabel('\alpha (deg)')
grid on
xlim([0 max(alfa)])
set(gca,'FontSize',12)

% Plot curvature radius
figure
subplot(2,1,1)
plot(alfa,x_rho,'LineWidth',1.5)
hold on
plot(alfa,x_rho0,'-.','LineWidth',1.5)
plot(alfa,Rr*ones(length(alfa),1),'-.','LineWidth',1.5)
legend('Cam curvature \rho','Pitch curvature \rho_0','Roller radius R_r')
ylabel('Curvature radii')
xlabel('\alpha (deg)')
grid on
xlim([0 max(alfa)])
set(gca,'FontSize',12)

subplot(2,1,2)
plot(alfa,x_rho.*x_rho0,'LineWidth',1.5)
hold on
plot(alfa(x_undercut < 0),x_undercut(x_undercut < 0),'-d','LineWidth',1.5)
legend('\rho \times \rho_0')%,'Undercut')
ylabel('Undercut checkup')
xlabel('\alpha (deg)')
grid on
xlim([0 max(alfa)])
set(gca,'FontSize',12)

% Plot cams
figure
plot(x_cam(:,1),x_cam(:,2),'-','LineWidth',1.5);
hold on
plot(x_pitch(:,1),x_pitch(:,2),'--','LineWidth',1.5);
scatter(0,0,'k','filled')
grid on
axis equal

%% Displacement cam
l = 15;
Rrr = 5;
beta0 = -pi/2;
beta = beta0 +q.pos'; 
q_pitch = [l*cos(beta) + x.pos', l*sin(beta)];
q_pitchp = [x.vel' - l*sin(beta).*q.vel', l*cos(beta).*q.vel'];
alfa2 = zeros(length(alfa),1);
for ii = 1:length(alfa)
    if q_pitchp(ii,1) == 0
        if ii == 1
            alfa2(ii) = 0;
        else
            alfa2(ii) = alfa2(ii-1);
        end
        
    else
        alfa2(ii) = atan(q_pitchp(ii,2)./(-q_pitchp(ii,1)));
    end
end
x_dcam = [q_pitch(:,1) - Rrr*sin(alfa2),q_pitch(:,2) - Rrr*cos(alfa2)];
x_ucam = [q_pitch(:,1) + Rrr*sin(alfa2),q_pitch(:,2) + Rrr*cos(alfa2)];

figure
plot(alfa,alfa2*180/pi)
grid on

figure
plot(q_pitch(:,1),q_pitch(:,2),'--','LineWidth',1.5);
hold on
plot(x_ucam(:,1),x_ucam(:,2),'-','LineWidth',1.5);
plot(x_dcam(:,1),x_dcam(:,2),'-','LineWidth',1.5);
%scatter(0,0,'k','filled')
%scatter(0,0,'k','filled')
grid on
axis equal

%% Saving data
posx = x.pos; postheta = q.pos;
fileID = fopen('motion.txt','w');
for ii = 1:length(x.pos)
    fprintf(fileID,'%6f \t %6f \t %6f \n',alfa(ii)*pi/180,x.pos(ii),q.pos(ii));%q.pos(ii));
end
fclose(fileID);

CAM = [alfa' x_cam(:,1) x_cam(:,2) zeros(length(x_cam),1)];
THETA = [alfa'/180/2/30 q.pos'];
save cam.dat CAM -ascii
save theta.dat THETA -ascii

