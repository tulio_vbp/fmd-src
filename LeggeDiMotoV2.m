function[out]=LeggeDiMotoV2(MotionLaw,h,Ab,N,xi)
    
    % Inputparameters
    %
    % MotionLaw = Identifier:
    % �ACS� : constant acceleration symmetric law
    % �TVP� : trapezoidal velocity profile
    % �CUB� : cubic law
    % �CIC� : cycloidal law
    % �Dwell� : dwell
    %
    % h = Lift
    % Ta = Time of advancement
    % N = Total number of discretization points
    % xi = vector array for the modified trapeizoidal motion 
    %
    % REMARK: The first three inputs must be of the same size
    %
    %

    if length(MotionLaw)~=length(h) || length(h)~=length(Ab)
        error('MotionLaw, h and Ta arrays must have the same size');
    end

    % Discretization
    t=linspace(0,sum(Ab),N);

    % Memory allocation
    out.pos=zeros(1,N);
    out.vel=zeros(1,N);
    out.acc=zeros(1,N);
    
    % Set of instructions at each discretization point
    k_prev = 1;
    for k=1:N
        % we need to know on which stage we are by returning the index of the abscissa vector
        CurInt=find(cumsum(Ab)>=t(k),1);
        if CurInt >=2 && PrevInt == (CurInt - 1)
            k_prev = k-1;
        end
        % we need to calculate epsilon
        epsilon=(t(k)-sum(Ab(1:(CurInt-1))))/Ab(CurInt);
        % switch-case statement to select the needed law
        switch char(MotionLaw(CurInt))
            case{'ACS'}
                AdimLaw=ACS(epsilon);
            case{'TVP'}
                AdimLaw=TVP(epsilon);
            case{'CUB'}
                AdimLaw=Cubic(epsilon);
            case{'CIC'}
                AdimLaw=Cycloidal(epsilon);
            case{'Dwell'}
                AdimLaw=Dwell(epsilon);
            case{'MTVP'}
                if exist('xi','var') == 0 || length(xi)~=7
                    xi = [1/8 1/4 1/8 0 1/8 1/4 1/8]; % Generic xi
                end
                AdimLaw=MTVP(epsilon,xi);
            otherwise
                error('%s motion law not recognized', char(MotionLaw(CurInt)));
        end
        % Output in terms of position, velocity and acceleration
         out.pos(k) = AdimLaw.s*h(CurInt) + out.pos(k_prev); % add previous displacement
         out.vel(k) = AdimLaw.v*h(CurInt)/Ab(CurInt);
         out.acc(k) = AdimLaw.a*h(CurInt)/Ab(CurInt)^2;
        % Check the last CurInt for updating the displacement
         PrevInt = CurInt;
    end
end

%% Adimensionalised motion laws

% ACS

function [out] = ACS(epsilon)
     ev = 1/2;
     ca = 4;
     cv = 2;
    if 0<=epsilon && epsilon <= ev
        out.a = ca; % Ca 
        out.v = ca*epsilon;
        out.s = 1/2*ca*epsilon^2;
    else
        out.a = -ca;
        out.v = ca*(1-epsilon);
        out.s = ca*(epsilon-epsilon^2/2 + ev - ev^2 -1/2);
    end
end

% TVP (1/3 ,1/3 ,1/3)
function [out]=TVP(epsilon)
        ev = 1/3;
        cv = 1.5;
        ca = 4.5;
    if 0<=epsilon && epsilon <= ev
        out.a = ca;
        out.v = ca*epsilon;
        out.s = 1/2*ca*epsilon^2;
    elseif ev<epsilon && epsilon <= (1-ev)
        out.a = 0;
        out.v = ca*ev;
        out.s = ca*ev*epsilon-1/2*ca*ev^2;
    else
        out.a = -ca;
        out.v = ca*(1-epsilon);
        out.s = ca*(epsilon-epsilon^2/2+ev-ev^2-1/2);
    end
end

% CUB
function[out]=Cubic(epsilon)
     out.a = 6*(1-2*epsilon);
     out.v = 6*epsilon*(1-epsilon);
     out.s = epsilon*(3*epsilon - 2*epsilon^2);
end

% CIC
function[out]=Cycloidal(epsilon)
    out.a = 2*pi*sin(2*pi*epsilon);
    out.v = 1 - cos(2*pi*epsilon);
    out.s = epsilon - 1/2/pi*sin(2*pi*epsilon);
end

% DWELL
function[out]=Dwell(epsilon)
    out.a = 0;
    out.v = 0;
    out.s = 0;
end

% Modified trapezoidal function . . .
function[out] = MTVP(epsilon,xi)
    xi = xi/sum(xi); % Normalize xi
    [ca,k] = constants(xi);
    if epsilon < xi(1)
        % Sinuisoidal acceleration
        de = 4*xi(1);
        out.a = ca(1)*sin(2*pi*epsilon/de);
        out.v =  -ca(1)*xi(1)*2/pi*cos(2*pi*epsilon/de) + k(1,1);
        out.s =  -ca(1)*(xi(1)*2/pi)^2*sin(2*pi*epsilon/de) + k(1,1)*epsilon + k(1,2);
    elseif epsilon < sum(xi(1:2))
        % Dwell
        epsilon = epsilon - xi(1);
        out.a = ca(1);
        out.v = ca(1)*epsilon+k(2,1);
        out.s = k(2,1)*epsilon + ca(1)/2*epsilon^2 + k(2,2);
    elseif epsilon < sum(xi(1:3))
        % Sinuisoidal deacceleration
        de = 4*xi(3);
        epsilon = epsilon - sum(xi(1:2));
        out.a = ca(1)*cos(2*pi*epsilon/de);
        out.v = ca(1)*2/pi*xi(3)*sin(2*pi*epsilon/de) + k(3,1);
        out.s = -ca(1)*(xi(3)*2/pi)^2*cos(2*pi*epsilon/de)+k(3,1)*epsilon+k(3,2);
    elseif epsilon < sum(xi(1:4))
        % Dwell
        epsilon = epsilon - sum(xi(1:3));
        out.a = 0;
        out.v = k(4,1);
        out.s = k(4,1)*epsilon + k(4,2);
    elseif epsilon < sum(xi(1:5))
        de = 4*xi(5);
        epsilon = epsilon - sum(xi(1:4));
        out.a = ca(2)*sin(2*pi*epsilon/de);
        out.v = -ca(2)*2/pi*xi(5)*cos(2*pi*epsilon/de) + k(5,1);
        out.s = -ca(2)*(xi(5)*2/pi)^2*sin(2*pi*epsilon/de) + k(5,1)*epsilon+k(5,2);
    elseif epsilon < sum(xi(1:6))
        epsilon = epsilon - sum(xi(1:5));
        out.a = ca(2);
        out.v = ca(2)*epsilon + k(6,1);
        out.s = ca(2)/2*epsilon^2 + k(6,1)*epsilon + k(6,2);
    elseif epsilon <= sum(xi(1:7))
        de = 4*xi(7);
        epsilon = epsilon - sum(xi(1:6));
        out.a = ca(2)*cos(2*pi*epsilon/de);
        out.v = ca(2)*xi(7)*2/pi*sin(2*pi*epsilon/de) + k(7,1);
        out.s = -ca(2)*(xi(7)*2/pi)^2*cos(2*pi*epsilon/de) + k(7,1)*epsilon + k(7,2);
    else
        disp("error");
        
    end
        
end

function [a,k]= constants(xi)
    M = [-2*xi(1)/pi 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0; % m11
         0 0 1 -1 0 0 0 0 0 0 0 0 0 0 0 0; % m2,1
         xi(2) 0 0 1 -1 0 0 0 0 0 0 0 0 0 0 0; % m3,1
         2*xi(3)/pi 0 0 0 1 -1 0 0 0 0 0 0 0 0 0 0; % m4,1
         0 2*xi(5)/pi 0 0 0 1 -1 0 0 0 0 0 0 0 0 0; % m5,2
         0 0 0 0 0 0 1 -1 0 0 0 0 0 0 0 0; % m6,2
         0 xi(6) 0 0 0 0 0 1 -1 0 0 0 0 0 0 0; % m7,2
         0 2*xi(7)/pi 0 0 0 0 0 0 1 0 0 0 0 0 0 0; % m8,2
         0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0; % m9,1
         -(2*xi(1)/pi)^2 0 xi(1) 0 0 0 0 0 0 1 -1 0 0 0 0 0; % m10,1
         xi(2)^2/2+(2*xi(3)/pi)^2 0 0 xi(2) 0 0 0 0 0 0 1 -1 0 0 0 0; %m11,1
         0 0 0 0 xi(3) 0 0 0 0 0 0 1 -1 0 0 0; % m12,1
         0 0 0 0 0 xi(4) 0 0 0 0 0 0 1 -1 0 0; % m13,2
         0 -(2*xi(5)/pi)^2 0 0 0 0 xi(5) 0 0 0 0 0 0 1 -1 0; % m14,2
         0 xi(6)^2/2+(2*xi(7)/pi)^2 0 0 0 0 0 xi(6) 0 0 0 0 0 0 1 -1; %m15,2
         0 0 0 0 0 0 0 0 xi(7) 0 0 0 0 0 0 1]; %m16,2
    q = zeros(16,1);
    q(end) = 1;
    ans = M\q;
    a = ans(1:2);
    k = [ans(3:9) ans(10:end)];
end